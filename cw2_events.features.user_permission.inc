<?php

/**
 * Implementation of hook_user_default_permissions().
 */
function cw2_events_user_default_permissions() {
  $permissions = array();

  // Exported permission: administer all signups
  $permissions['administer all signups'] = array(
    'name' => 'administer all signups',
    'roles' => array(
      '0' => 'TB Primary Coordinator',
      '1' => 'TBUSA site admin',
    ),
  );

  // Exported permission: administer signups for own content
  $permissions['administer signups for own content'] = array(
    'name' => 'administer signups for own content',
    'roles' => array(
      '0' => 'Member',
      '1' => 'TB Asst Coordinator',
      '2' => 'TB Coordinator',
      '3' => 'TB Primary Coordinator',
      '4' => 'TBUSA site admin',
    ),
  );

  // Exported permission: cancel own signups
  $permissions['cancel own signups'] = array(
    'name' => 'cancel own signups',
    'roles' => array(
      '0' => 'Member',
      '1' => 'TB Asst Coordinator',
      '2' => 'TB Coordinator',
      '3' => 'TB Primary Coordinator',
      '4' => 'TBUSA site admin',
    ),
  );

  // Exported permission: cancel signups
  $permissions['cancel signups'] = array(
    'name' => 'cancel signups',
    'roles' => array(
      '0' => 'Member',
      '1' => 'TB Asst Coordinator',
      '2' => 'TB Coordinator',
      '3' => 'TB Primary Coordinator',
      '4' => 'TBUSA site admin',
    ),
  );

  // Exported permission: edit own signups
  $permissions['edit own signups'] = array(
    'name' => 'edit own signups',
    'roles' => array(
      '0' => 'Member',
      '1' => 'TB Asst Coordinator',
      '2' => 'TB Coordinator',
      '3' => 'TB Primary Coordinator',
      '4' => 'TBUSA site admin',
    ),
  );

  // Exported permission: email all signed up users
  $permissions['email all signed up users'] = array(
    'name' => 'email all signed up users',
    'roles' => array(
      '0' => 'TB Asst Coordinator',
      '1' => 'TB Coordinator',
      '2' => 'TB Primary Coordinator',
      '3' => 'TBUSA site admin',
    ),
  );

  // Exported permission: email users signed up for own content
  $permissions['email users signed up for own content'] = array(
    'name' => 'email users signed up for own content',
    'roles' => array(
      '0' => 'TB Asst Coordinator',
      '1' => 'TB Coordinator',
      '2' => 'TB Primary Coordinator',
      '3' => 'TBUSA site admin',
    ),
  );

  // Exported permission: sign up for content
  $permissions['sign up for content'] = array(
    'name' => 'sign up for content',
    'roles' => array(
      '0' => 'Member',
      '1' => 'TB Asst Coordinator',
      '2' => 'TB Coordinator',
      '3' => 'TB Primary Coordinator',
      '4' => 'TBUSA site admin',
    ),
  );

  // Exported permission: view all signups
  $permissions['view all signups'] = array(
    'name' => 'view all signups',
    'roles' => array(
      '0' => 'Member',
      '1' => 'TB Asst Coordinator',
      '2' => 'TB Coordinator',
      '3' => 'TB Primary Coordinator',
      '4' => 'TBUSA site admin',
    ),
  );

  return $permissions;
}
